provider "aws" {
  profile     = var.profile
  region      = var.region-common
  alias       = "region-common"

}

provider "aws" {
  profile     = var.profile
  region      = var.region-worker
  alias       = "region-worker"
}

data "aws_availability_zones" "available" {}


terraform {
  required_version    =    ">= 0.12.0"
  required_providers {
    aws = ">=3.0.0"
  }

  backend "s3" {
    region            = var.region
    profile           = var.profile
    key               = var.key
    bucket            = var.bucket
  }
}

data "terraform_remote_state" "backend" {
  backend = "s3"
  config = {
    bucket = var.bucket
    region = var.region
    profile = var.profile
    key = var.key-backend
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"
  config = {
    bucket = var.bucket
    region = var.region
    profile = var.profile
    key = var.key-network
  }
}


#Create SG for LB, only for TCP/80,443 and outbound access
resource "aws_security_group" "elb-sg" {
  provider                 = aws.region-common
  name                     = "ELB-SG"
  description              = "Allow 443 and traffic to jenkins sg"
  vpc_id                   = data.terraform_remote_state.network.aws_vpc.vpc_common

  dynamic "ingress" {
    for_each    = [
      "443",
      "80"]
    content {
      from_port            = ingress.value
      to_port              = ingress.value
      protocol             = "tcp"
      description          = "Allow 80,443 from anywhere for redirection"
      cidr_blocks          = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port              = 0
    protocol               = "-1"
    to_port                = 0
    cidr_blocks            = ["0.0.0.0/0"]
  }
  tags    = {
    Name                   = "Common ELB SG for jenkins"
    Owner                  = "Aleksandr Andreichenko"
    Environmet             = "Dev-Test"
    Region                 = "us-east-1"
  }
}
