provider "aws" {
  profile     = var.profile
  region      = var.region-common
  alias       = "region-common"

}

provider "aws" {
  profile     = var.profile
  region      = var.region-worker
  alias       = "region-worker"
}

data "aws_availability_zones" "available" {}

terraform {
  required_version    =    ">= 0.12.0"
  required_providers {
    aws = ">=3.0.0"
  }

  backend "s3" {
    region            = var.region
    profile           = "default"
    key               = var.key
    bucket            = var.bucket
  }
}



