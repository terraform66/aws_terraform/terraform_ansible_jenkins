variable "bucket" {
  type = string
  default = "terraform-state-bucket-frei-0009"
}

variable "region" {
  type = string
  default = "us-east-1"
}

variable "key" {
  type = string
  default = "terraform-state-file/backend.tfstate"
}

variable "profile" {
  type          = string
  default       = "default"
}

variable "region-common" {
  type          = string
  default       = "us-east-1"
}

variable "region-worker" {
  type          = string
  default       = "us-west-2"
}