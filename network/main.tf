provider "aws" {
  profile     = var.profile
  region      = var.region-common
  alias       = "region-common"

}

provider "aws" {
  profile     = var.profile
  region      = var.region-worker
  alias       = "region-worker"
}

data "aws_availability_zones" "available" {}

terraform {
  required_version    =    ">= 0.12.0"
  required_providers {
    aws = ">=3.0.0"
  }

  backend "s3" {
    region            = var.region
    profile           = var.profile
    key               = var.key
    bucket            = var.bucket
  }
}

data "terraform_remote_state" "backend" {
  backend = "s3"
  config = {
    bucket = var.bucket
    region = var.region
    profile = var.profile
    key = var.key-backend
  }
}

# Creating VPC for us-east-1
resource "aws_vpc" "vpc_common" {
  provider                      = aws.region-common
  cidr_block                    = "10.0.0.0/16"
  enable_dns_hostnames          = true
  enable_dns_support            = true
  tags    = {
    Name                        = "common-vpc-jenkins"
    Owner                       = "Aleksandr Andreichenko"
    Environment                 = "Dev-Test"
    Region                      = "us-east-1"
  }
}

#Creating VPC for us-west-2
resource "aws_vpc" "vpc_common_oregon" {
  provider                      = aws.region-worker
  cidr_block                    = "192.168.0.0/16"
  enable_dns_hostnames          = true
  enable_dns_support            = true
  tags    = {
    Name                        = "worker-vpc-jenkins"
    Owner                       = "Aleksandr Andreichenko"
    Environment                 = "Dev-Test"
    Region                      = "us-west-2"
  }
}

#Create and Initiate peering connection request from us-east-1
resource "aws_vpc_peering_connection" "east-west" {
  peer_vpc_id                   = aws_vpc.vpc_common_oregon.id
  vpc_id                        = aws_vpc.vpc_common.id
  provider                      = aws.region-common
  peer_region                   = var.region-worker
}

#Create Internet Gateway in us-east-1
resource "aws_internet_gateway" "internet-gateway-common" {
  provider                      = aws.region-common
  vpc_id                        = aws_vpc.vpc_common.id
  tags    = {
    Name                        = "Common IGW"
    Owner                       = "Aleksandr Andreichenko"
    Environmet                  = "Dev-Test"
    Region                      = "us-east-1"
  }
}

#Create Internet Gateway in us-west-2
resource "aws_internet_gateway" "internet-gateway-worker" {
  provider                      = aws.region-worker
  vpc_id                        = aws_vpc.vpc_common_oregon.id
  tags    = {
    Name                        = "Worker IGW"
    Region                      = "us-west-2"
    Owner                       = "Aleksandr Andreichenko"
    Environmet                  = "Dev-Test"
  }
}

#Accept VPC peering request in west to east
resource "aws_vpc_peering_connection_accepter" "accept_peering" {
  provider                      = aws.region-worker
  auto_accept                   = true
  vpc_peering_connection_id     = aws_vpc_peering_connection.east-west.id
}

#Create route table in east
resource "aws_route_table" "internet_route" {
  provider                      = aws.region-common
  vpc_id                        = aws_vpc.vpc_common.id
  route {
    cidr_block                  = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gateway-common.id
  }
  route {
    cidr_block                  = "192.168.1.0/24"
    vpc_peering_connection_id   = aws_vpc_peering_connection.east-west.id
  }
  lifecycle {
    ignore_changes              = all
  }
  tags        = {
    Name                        = "Common-Regionr-Route-table"
  }
}

#Owerwrite default route table of VPC common with our route table entries
resource "aws_main_route_table_association" "set-common-worker-rt-associate" {
  route_table_id                = aws_route_table.internet_route.id
  vpc_id                        = aws_vpc.vpc_common.id
  provider                      = aws.region-common
}

#Get all available AZ's in VPC for common
data "aws_availability_zones" "azs" {
  provider                      = aws.region-common
  state                         = "available"
}

#Create subnet for common VPC
resource "aws_subnet" "common_subnet_primary" {
  provider                      = aws.region-common
  cidr_block                    = "10.0.1.0/24"
  availability_zone             = element(data.aws_availability_zones.azs.names, 0)
  vpc_id                        = aws_vpc.vpc_common.id
  tags          = {
    Name                        = "Common subnet primary"
    Owner                       = "Aleksandr Andreichenko"
    Environmet                  = "Dev-Test"
    Region                      = "us-east-1"
  }
}

resource "aws_subnet" "common_subnet_secondary" {
  cidr_block                    = "10.0.2.0/24"
  vpc_id                        = aws_vpc.vpc_common.id
  provider                      = aws.region-common
  availability_zone             = element(data.aws_availability_zones.azs.names, 1)
  tags    = {
    Name                        = "Common subnet secondary"
    Owner                       = "Aleksandr Andreichenko"
    Environmet                  = "Dev-Test"
    Region                      = "us-east-1"
  }
}

#Create worker subnet for us-west-2
resource "aws_subnet" "worker_subnet" {
  provider                      = aws.region-worker
  cidr_block                    = "192.168.1.0/24"
  vpc_id                        = aws_vpc.vpc_common_oregon.id
  tags    = {
    Name                        = "Worker subnet"
    Owner                       = "Aleksandr Andreichenko"
    Environmet                  = "Dev-Test"
    Region                      = "us-west-2"
  }
}

#Create route table in west
resource "aws_route_table" "internet_route_oregon" {
  vpc_id                        = aws_vpc.vpc_common_oregon.id
  provider                      = aws.region-worker
  route {
    cidr_block                  = "0.0.0.0/0"
    gateway_id                  = aws_internet_gateway.internet-gateway-worker.id
  }
  route {
    cidr_block                  = "10.0.1.0/24"
    vpc_peering_connection_id   = aws_vpc_peering_connection.east-west.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name                        = "worker-region-route-table"
  }
}

#Owerwrite default route table of VPC worker with our route table entries
resource "aws_main_route_table_association" "set-worker-default-rt-associate" {
  route_table_id                = aws_route_table.internet_route_oregon.id
  vpc_id                        = aws_vpc.vpc_common_oregon.id
  provider                      = aws.region-worker
}

#create ALB for jenkins application
resource "aws_lb" "application-load-balancer" {
  provider                         = aws.region-common
  name                             = "jenkins-alb"
  internal                         = false
  load_balancer_type               = "application"
  security_groups                  = [aws_security_group.elb-sg.id]
  subnets                          = [aws_subnet.common_subnet_primary.id, aws_subnet.common_subnet_secondary.id]
  tags = {
    Name      = "Jenkins_ALB"
  }
}
#Create target group
resource "aws_lb_target_group" "app-lb-tg" {
  provider                         = aws.region-common
  name                             = "app-lb-tg"
  port                             = var.webserver-port
  target_type                      = "instance"
  vpc_id                           = aws_vpc.vpc_common.id
  protocol                         = "HTTP"
  health_check {
    enabled                        = true
    interval                       = 10
    path                           = "/login"
    port                           = var.webserver-port
    protocol                       = "HTTP"
    matcher                        = "200-299"
  }
  tags         = {
    Name                           = "jenkins_target_group"
  }
}

resource "aws_lb_listener" "jenkins-listener" {
  provider                         = aws.region-common
  load_balancer_arn                = aws_lb.application-load-balancer.arn
  ssl_policy                       = "ELBSecurityPolicy-2016-08"
  port                             = 443
  protocol                         = "HTTPS"
  certificate_arn                  = aws_acm_certificate.jenkins-lb-https.arn
  default_action {
    type         = "forward"
    target_group_arn               = aws_lb_target_group.app-lb-tg.arn
  }
}

resource "aws_lb_listener" "jenkins-listener-http" {
  provider                         = aws.region-common
  load_balancer_arn                = aws_lb.application-load-balancer.arn
  port                             = 80
  protocol                         = "HTTP"
  default_action {
    type         = "redirect"
    redirect {
      port                         = "443"
      status_code                  = "HTTP_301"
      protocol                     = "HTTPS"
    }
  }
}

resource "aws_lb_target_group_attachment" "jenkins-master-attachment" {
  provider                         = aws.region-common
  target_group_arn                 = aws_lb_target_group.app-lb-tg.arn
  target_id                        = aws_instance.jenkins-master-node.id
  port                             = var.webserver-port
}