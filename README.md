# Jenkins master and worker solution unsing terraform and ansible

This automation is for installing and deploying Jenkins CI with a specific configuration that can be changed. The first version of the solution is now ready, which only works in AWS. It is planned to add modules for the multicloud infrastructure, which includes modules. I have prepared some diagram that will give you an understanding of the architecture.

![alt text](https://user-images.githubusercontent.com/20015341/113284469-58884a80-92f2-11eb-8ada-dcde2904004c.png)
